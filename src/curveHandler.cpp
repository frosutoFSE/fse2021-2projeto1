#include "../inc/curveHandler.hpp"
#include <ctime>
#include <cstdio>
#include <unistd.h>
#include <vector>
#include <iterator>
#include <utility>

namespace refcurve {

static FILE* fd = NULL;
static std::vector<std::pair<int, float>>* tt = nullptr;
static std::vector<std::pair<int, float>>::iterator it;

// static bool IsCurveModeSet = false;
static time_t start;
static time_t now;


int setup(char* filename) {
    fd = fopen(filename, "r");
    if (fd == NULL) {
        return -1;
    }
    tt = new std::vector<std::pair<int, float>>();
    return 0;
}

void close() {
    if (tt != nullptr) delete tt;
}

void readFile() {
    char buffer[20];
    int time;
    float temp;
    fscanf(fd, " %[^\n]", buffer);
    while (fscanf(fd, " %d, %f", &time, &temp) != EOF) {
        tt->emplace_back(std::pair<int, float> (time, temp));
    }
    fclose(fd);
}

float getReferenceValue() {
    time(&now);

    std::vector<std::pair<int, float>>::iterator nextit = it == tt->end() ? it : std::next(it, 1);

    if ((int)difftime(now, start) >= nextit->first) {
        if (std::next(it, 1) != tt->end()) {
            it++;
            return it->second;
        }
    }
    return it->second;
}

void startTimer() {
    time(&start);
    it = tt->begin();
}

// void setCurveMode(bool flag) {
//     if (not IsCurveModeSet && flag) {
// //         currentRef = tt[0].temp;
//         time(&start);
// //         it = tt->begin();
// //         IsCurveModeSet = flag;
//     }
//     else if (IsCurveModeSet && not flag){
//         IsCurveModeSet = flag;
//     }
// }

}

