#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#include "../inc/bme280.h"
#include "../inc/bme280_handler.h"

// Pascal(Pa) to hectopascal(hPa)
#define PA_TO_HPA(n) n/100.00f

static const char* i2c_file = "/dev/i2c-1";
static int8_t fd = -1;
static struct bme280_dev dev;

int8_t i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void* intf_ptr) {
    write(fd, &reg_addr, 1);
    read(fd, data, len);
    return BME280_OK;
}

int8_t i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr) {
    uint8_t *buffer;

    buffer = malloc(len+1);
    buffer[0] = reg_addr;

    memcpy(buffer+1, data, len);
    if (write(fd, buffer, len+1) < (uint16_t)len) {
        return BME280_E_COMM_FAIL;
    }

    free(buffer);
    return BME280_OK;
}

void i2c_delay(uint32_t period, void *intf_ptr) {
    usleep(period);
}


int8_t set_fd() {
    fd = open(i2c_file, O_RDWR);
    if (fd < 0) {
        return -1;
    }
    return 0;
}

int set_i2c_slave() {
    if (ioctl(fd, I2C_SLAVE, BME280_I2C_ADDR_PRIM) < 0) {
//         printf("Erro ao conectar ao barramento\n");
        return -1;
    }
    return 0;
}

int BMEH_setup() {
    if (set_fd() != 0) {
        return -1;
    }

    int8_t result = BME280_OK;

    dev.intf = BME280_I2C_INTF;
    dev.read = i2c_read;
    dev.write = i2c_write;
    dev.delay_us = i2c_delay;

    if (set_i2c_slave() < 0) {
        return -1;
    }

    result = bme280_init(&dev);
    if (result != BME280_OK) {
        return -1;
    }

    return 0;
}

void BMEH_close() {
    if (fd > 0) close(fd);
}

int BMEH_configure_sensor_to_normal_mode() {
    int8_t result;
    uint8_t settings_sel;

    dev.settings.osr_h = BME280_OVERSAMPLING_1X;
    dev.settings.osr_p = BME280_OVERSAMPLING_16X;
    dev.settings.osr_t = BME280_OVERSAMPLING_2X;
    dev.settings.filter = BME280_FILTER_COEFF_16;
    dev.settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

    settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_STANDBY_SEL | BME280_FILTER_SEL;

    result = bme280_set_sensor_settings(settings_sel, &dev);

    if (result != BME280_OK) {
//         Erro ao setar as configurações
        return result;
    }

    result = bme280_set_sensor_mode(BME280_NORMAL_MODE, &dev);

    if (result != BME280_OK) {
//         Erro ao setar o modo do sensor
        return result;
    }

    return result;
}

int8_t get_sensor_data(struct bme280_data* data) {
    return bme280_get_sensor_data(BME280_ALL, data, &dev);
}

int BMEH_get_sensor_data(float* temperature, float* pressure, float* humidity) {
    struct bme280_data data;
    int8_t result = get_sensor_data(&data);
    if (result != BME280_OK) {
        return result;
    }

    if (temperature != NULL) {
        *temperature = data.temperature;
    }

    if (pressure != NULL) {
        *pressure = PA_TO_HPA(data.pressure);
    }

    if (humidity != NULL) {
        *humidity = data.humidity;
    }

    return result;
}
