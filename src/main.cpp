#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <ctime>
#include <utility>
#include <vector>
#include <string>
#include <unistd.h>

#include <signal.h>
#include <wiringPi.h>

extern "C" {
#include "../inc/i2clcd.h"
#include "../inc/pid.h"
#include "../inc/bme280_handler.h"
}

#include "../inc/actuators.hpp"
#include "../inc/application_uart_calls.hpp"
#include "../inc/logger.hpp"
#include "../inc/curveHandler.hpp"

// User commands
#define NOC 0x00
#define ON  0x01
#define OFF 0x02
#define POT 0x03
#define CUR 0x04

//Modos
#define SYSTEMON   1
#define SYSTEMOFF  0
#define CMCUR 1
#define CMPOT 0

static char mat[4];
char* curveFile;

CSVLogger* logger = NULL;
const std::string loggerFile = "./log.csv";
std::vector<std::string> headersLogger = {"Data", "Hora", "Modo de Controle", "Temperatura Ambiente", "Temperatura Interna", "Temperatura Referencia", "Acionamento %"};

typedef std::pair<std::string, std::string> logPair;

struct PID {
    double kp;
    double ki;
    double kd;
};

void getMatricula() {
    bool matRead = false;
    while (not matRead) {
        memset(mat, 'a', 4);

        printf("Digite os 4 digitos finais de sua matrícula:\n> ");
        scanf(" %s", mat);

        for (int i = 0; i < 4; i++) {
            if (isdigit(mat[i]) != 0) {
                matRead = true;
            }
            else {
                printf("Erro na leitura da matrícula. Por favor, tente novamente.\n");
                matRead = false;
                break;
            }
        }
    }
}

void initComponent(int status, std::string componentName) {
    switch (status) {
        case (0):
            return;
        default:
            printf("Falha na inicialização do componente: %s\n", componentName.c_str());
            exit(1);
    }
}

void initLogger() {
    logger = new CSVLogger(headersLogger);
    initComponent(logger->openFile(loggerFile), "Logger");
}

void setLcdOff() {
    ClrLcd();
    lcdLoc(LINE1);
    typeln((char*)"SYSTEM OFF");
}

void pidSetup() {
    const struct PID rasp42 = {.kp = 30.0, .ki = 0.2, .kd = 400.0};
    const struct PID rasp43 = {.kp = 20.0, .ki = 0.1, .kd = 100.0};
    struct PID userDefined = {0, 0, 0};
    int perfil = 0;

    while (true) {
        printf("Selecione o perfil (codigo) para constantes de pid:\n");
        printf("1: rasp42 {Kp = %.2lf, Ki = %.2lf, kd = %.2lf}\n", rasp42.kp, rasp42.ki, rasp42.kd);
        printf("2: rasp43 {Kp = %.2lf, Ki = %.2lf, kd = %.2lf}\n", rasp43.kp, rasp43.ki, rasp43.kd);
        printf("3: definir manualmente\n> ");
        scanf(" %d", &perfil);

        if (perfil < 1 && perfil > 3) {
            printf("Perfil inválido.\n");
            continue;
        }
        break;
    }

    switch(perfil) {
        case (1):
            pid_configura_constantes(rasp42.kp, rasp42.ki, rasp42.kd);
            printf("Perfil configurado: rasp42\n");
            printf("{Kp = %.2lf, Ki = %.2lf, kd = %.2lf}\n", rasp42.kp, rasp42.ki, rasp42.kd);
            break;
        case (2):
            pid_configura_constantes(rasp43.kp, rasp43.ki, rasp43.kd);
            printf("Perfil configurado: rasp43\n");
            printf("{Kp = %.2lf, Ki = %.2lf, kd = %.2lf}\n", rasp43.kp, rasp43.ki, rasp43.kd);
            break;
        case (3):
            printf("Digite os valores das constantes:\n");
            printf("Kp> ");
            scanf(" %lf", &userDefined.kp);
            printf("Ki> ");
            scanf(" %lf", &userDefined.ki);
            printf("Kd> ");
            scanf(" %lf", &userDefined.kd);

            printf("Perfil configurado: definido pelo usuário\n");
            printf("{Kp = %.2lf, Ki = %.2lf, kd = %.2lf}\n", userDefined.kp, userDefined.ki, userDefined.kd);
    }
}

void initComponents() {
    printf("Iniciando componentes...\n");

    wiringPiSetup();

    initComponent(BMEH_setup(), "Sensor BME280");
    initComponent(BMEH_configure_sensor_to_normal_mode(), "Modo de operacao BME280");

    initComponent(lcd_setup(), "I2C - LCD");
    setLcdOff();

    initComponent(act::setup(), "Atuadores");

    initLogger();

    getMatricula();
    initComponent(uart::init(mat), "UART");

    initComponent(refcurve::setup(curveFile), "Arquivo de leitura da curva de temperatura");
    refcurve::readFile();
}

void closeAll(int s) {
    printf("Desligando...\n");

    BMEH_close();;
    setLcdOff();
    act::close();

    uart::sendControlSignal(0);
    uart::close();
    if (logger != NULL) delete logger;
    refcurve::close();

    struct sigaction sa;
    sa.sa_handler = SIG_DFL;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    sigaction(s, &sa, NULL);
    raise(s);
}

void updateLcd(std::string mode, float tempRef, float tempInt, float tempExt) {
    lcdLoc(LINE1);
    typeln("M:");
    typeln(mode.c_str());
    typeChar(' ');

    typeln("TR");
    typeFloat(tempRef);

    lcdLoc(LINE2);
    typeln("TI");
    typeFloat(tempInt);

    typeChar(' ');
    typeln("TE");
    typeFloat(tempExt);
}

std::pair<std::string, std::string> getFormatedDateAndTime() {
    char currentDate[20];
    char currentTime[20];

    memset(currentDate, 0, 20);
    memset(currentTime, 0, 20);

    time_t now;
    time(&now);

    struct tm* local = localtime(&now);

    strftime(currentDate, 20, "%d/%m/%y", local);
    strftime(currentTime, 20, "%T", local);

    return std::pair<std::string, std::string>(std::string(currentDate), std::string(currentTime));
}

void updateLog(std::string mode, float tempRef, float tempInt, float tempExt, int acionamento) {
    std::pair<std::string, std::string> dateTime = getFormatedDateAndTime();
    std::vector<logPair> info = {
        {"Data", dateTime.first},
        {"Hora", dateTime.second},
        {"Modo de Controle", mode},
        {"Temperatura Ambiente", convertToString<float>(tempExt)},
        {"Temperatura Interna", convertToString<float>(tempInt)},
        {"Temperatura Referencia", convertToString<float>(tempRef)},
        {"Acionamento %", convertToString<int>(acionamento)}
    };

    logger->setContent(info);
    logger->logContent();
}

void potentiometerHandle() {
    // pega referencia do potenciometro
    float tempRef = uart::requestPotentiometerTemperature();

    // pega temperatura interna
    float tempInt = uart::requestInternalTemperature();

    // calcula pid
    pid_atualiza_referencia(tempRef);
    int controlSignal = (int)pid_controle(tempInt);

    // envia intensidade para os atuadores
    act::updateIntensity(controlSignal);

    // envia sinal de controle para a esp
    uart::sendControlSignal(controlSignal);

    // pega temparatura da bme280
    float tempExt;
    BMEH_get_sensor_data(&tempExt, NULL, NULL);

    // atualiza lcd
    updateLcd("POT", tempRef, tempInt, tempExt);

    // log
    updateLog("POT", tempRef, tempInt, tempExt, controlSignal);
}

void curveHandle() {
    // pega temperatura interna
    float tempInt = uart::requestInternalTemperature();

    // pega referencia da curva
    float tempRef = refcurve::getReferenceValue();

    // calcula pid
    pid_atualiza_referencia(tempRef);
    int controlSignal = (int)pid_controle(tempInt);

    // envia intensidade para os atuadores
    act::updateIntensity(controlSignal);

    // envia sinal de referencia para a esp
    uart::sendReferenceTemperature(tempRef);

    // envia sinal de controle (intensidade) para a esp
    uart::sendControlSignal(controlSignal);

    // pega temperatura da bme280
    float tempExt;
    BMEH_get_sensor_data(&tempExt, NULL, NULL);

    // atualiza lcd
    updateLcd("CUR", tempRef, tempInt, tempExt);

    // log
    updateLog("CUR", tempRef, tempInt, tempExt, controlSignal);
}

int main(int argc, char** argv) {

    if (argc < 2) {
        printf("Uso: %s <caminho para o arquivo com a curva de referencia>", argv[0]);
        exit(1);
    }

    curveFile = argv[1];
    initComponents();

    struct sigaction sa;
    sa.sa_handler = closeAll;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    initComponent(sigaction(SIGINT, &sa, NULL), "Handler do sinal SIGINT");

    pidSetup();

    int systemState = uart::sendSystemState(SYSTEMOFF);
    int controlMode = uart::sendControlMode(CMPOT);
    int usrcmd = NOC;

    bool curveTimerIsActive = false;

    while(true) {
        // Os timers para a espera das respostas estão configurados dentro dos módulos.
        usrcmd = uart::requestUserCommand();

        switch (usrcmd) {

            case (ON):
                if (systemState != ON) {
                    systemState = uart::sendSystemState(SYSTEMON);
                }
                break;

            case (OFF):
                if (systemState != OFF) {
                    systemState = uart::sendSystemState(SYSTEMOFF);
                }
                break;

            case (POT):
                if (controlMode != POT) {
                    controlMode = uart::sendControlMode(CMPOT);
                }
                break;

            case (CUR):
                if (controlMode != CUR) {
                    controlMode = uart::sendControlMode(CMCUR);
                }
                break;
        }

        switch (systemState) {
            case (SYSTEMON):
                if (controlMode == CMPOT) {
                    curveTimerIsActive = false;
                    potentiometerHandle();
                }
                else {
                    if (!curveTimerIsActive) {
                        refcurve::startTimer();
                        curveTimerIsActive = true;
                    }
                    curveHandle();
                }
                break;
            case (SYSTEMOFF):
                act::updateIntensity(0);
                uart::sendControlSignal(0);
                setLcdOff();
                break;
        }

    }

    return 0;
}
