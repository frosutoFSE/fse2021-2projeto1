#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "../inc/uart.hpp"

static UART* uart = NULL;

int UARTinit() {
    uart = new UART();
    return uart->init();
}

int UARTsend(void* message, size_t nbyte) {
    return uart->send(message, nbyte);
}

int UARTrecv(void* buffer, size_t nbyte) {
    return uart->recv(buffer, nbyte);
}

void UARTclose() {
    if (uart != NULL) delete uart;
}

UART::UART() {
}

UART::~UART() {
    close(fs);
}

int UART::init() {
    fs = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);

    if (fs == -1) {
        printf("Erro - Não foi possível iniciar a UART.\n");
        return -1;
    }
    else {
        printf("UART inicializada\n");
    }

    config(fs);
    return 0;
}

void UART::config(int fs) {
    tcgetattr(fs, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(fs, TCIFLUSH);
    tcsetattr(fs, TCSANOW, &options);
}

int UART::send(void* message, size_t nbyte) {
    return write(fs, message, nbyte);
}

int UART::recv(void* buffer, size_t nbyte) {
    return read(fs, buffer, nbyte);
}
