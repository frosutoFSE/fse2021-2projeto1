#include <cstdio>
#include <cstdlib>
#include <string>
#include <unistd.h>

#include "../inc/application_uart_calls.hpp"
#include "../inc/packet.hpp"
#include "../inc/modbus.hpp"
#include "../inc/uart.hpp"

#define USR_ADDR 0x00

static char final_mat[4];

/*
 * Envia os dados via UART e retorna o modbus para reuso
 */
template <DataType DT>
Modbus* sendData(FuncCode fc, Code packetCode, void* data) {
    Packet *p = Packet::createPacket(DT, data, final_mat, packetCode);

    size_t packLen = p->pack();

    Modbus* m = new Modbus(fc, p->getMessage(), packLen);
    size_t modLen = m->pack();

    UARTsend(m->getMessage(), modLen);

    delete p;
    return m;
}

void sendData(Modbus* m) {
    UARTsend(m->getMessage(), m->getMessageLen());
}

int verifyModbusError(Modbus* m, char expAddr, char expFunc) {
    return m->verifyHeader(expAddr, expFunc);
}

int verifyPacketCode(Code expPacketCode, char* packetBuffer) {
//     Criar o packet e recuperar a informação seria melhor do ponto de vista de modularidade, mas alocação demora :)
    return expPacketCode == packetBuffer[0];
}

Modbus* receiveData(FuncCode expFunc, Code expPacketCode, int* error) {
    char buffer[MODBUSMAXLEN];
    memset(buffer, 0, MODBUSMAXLEN);

    size_t rx_len = UARTrecv(buffer, MODBUSMAXLEN);

    Modbus* m = new Modbus(buffer, rx_len);

    *error = verifyModbusError(m, USR_ADDR, expFunc) || !verifyPacketCode(expPacketCode, m->getDataBuffer());
    return m;
}

template<typename T>
T extractPacketDataFromModbus(Modbus* m) {
    Packet *p = Packet::unpack(m->getDataBuffer());
    T data;
    p->getData(&data);
    delete p;
    return data;
}

template <DataType DT>
Modbus* sendAndReceive(FuncCode funcCode, Code packetCode, void* data = NULL, int expectsReturn = 1) {
    if (!expectsReturn) {
        sendData<DT>(funcCode, packetCode, data);
//         sleep(1);
        return NULL;
    }

    Modbus* tempSendModbus = sendData<DT>(funcCode, packetCode, data);
    Modbus* tempReceiveModbus;
    int error = 1;

    while (error) {
        sleep(1);

        tempReceiveModbus = receiveData(funcCode, packetCode, &error);
        if (error) {
            sendData(tempSendModbus);
            delete tempReceiveModbus;
        }
    }
    delete tempSendModbus;
    return tempReceiveModbus;
}

namespace uart {

int init(char* mat) {
    memcpy(final_mat, mat, 4);
    return UARTinit();
}

void close() {
    UARTclose();
}

float requestPotentiometerTemperature() {
    const FuncCode funcCode = SOLICITACAO;
    const Code packetCode = TPR;

    Modbus* tempReceiveModbus = sendAndReceive<NONE>(funcCode, packetCode);

    float data = extractPacketDataFromModbus<float>(tempReceiveModbus);
    delete tempReceiveModbus;
    return data;
}

float requestInternalTemperature() {
    const FuncCode funcCode = SOLICITACAO;
    const Code packetCode = TIR;

    Modbus* tempReceiveModbus = sendAndReceive<NONE>(funcCode, packetCode);

    float data = extractPacketDataFromModbus<float>(tempReceiveModbus);
    delete tempReceiveModbus;
    return data;
}

int requestUserCommand() {
    const FuncCode funcCode = SOLICITACAO;
    const Code packetCode = USR;

    Modbus* tempReceiveModbus = sendAndReceive<NONE>(funcCode, packetCode);

    int data = extractPacketDataFromModbus<int>(tempReceiveModbus);
    delete tempReceiveModbus;
    return data;
}

void sendControlSignal(int controlSignal) {
    const FuncCode funcCode = ENVIO;
    const Code packetCode = SCR;

    sendAndReceive<INT>(funcCode, packetCode, &controlSignal, 0);

    return;
}

void sendReferenceTemperature(float TR) {
    const FuncCode funcCode = ENVIO;
    const Code packetCode = STR;

    sendAndReceive<FLOAT>(funcCode, packetCode, &TR, 0);

    return;
}

int sendSystemState(char st) {
    const FuncCode funcCode = ENVIO;
    const Code packetCode = SST;

    Modbus* tempReceiveModbus = sendAndReceive<BYTE>(funcCode, packetCode, &st);

    int data = extractPacketDataFromModbus<int>(tempReceiveModbus);
    delete tempReceiveModbus;
    return data;
}

int sendControlMode(char mode) {
    const FuncCode funcCode = ENVIO;
    const Code packetCode = CMO;

    Modbus* tempReceiveModbus = sendAndReceive<BYTE>(funcCode, packetCode, &mode);

    int data = extractPacketDataFromModbus<int>(tempReceiveModbus);
    delete tempReceiveModbus;
    return data;
}

}
