#include <cstring>
#include <cstdlib>
#include <cstdio>

#include <vector>
#include <utility>
#include <unordered_map>
#include <string>

#include "../inc/logger.hpp"

CSVLogger::CSVLogger() {
    headerWritten = false;
}

CSVLogger::CSVLogger(std::vector<std::string>& headers) {
    headerWritten = false;
    setHeaders(headers);
}

CSVLogger::~CSVLogger() {
    fflush(fd);
    fclose(fd);
}

int CSVLogger::openFile(std::string filepath, char* mode) {
    path = filepath;
    fd = fopen(path.c_str(), mode);
    if (fd == NULL) {
        return -1;
    }
    return 0;
}

void CSVLogger::setHeaders(std::vector<std::string>& headers) {
    keys.clear();
    for (auto& h : headers) {
        keys.push_back(h);
        umap[h] = "";
    }
}

// void CSVLogger::setContent(std::unordered_map<std::string, std::string>& content) {
//     umap = content;
// }

void CSVLogger::setContent(std::pair<std::string, std::string>& content) {
    umap[content.first] = content.second;
}

void CSVLogger::setContent(std::vector<std::pair<std::string, std::string>>& content) {
    for (auto& kv : content) {
        setContent(kv);
    }
}

void CSVLogger::logHeader() {
    std::vector<std::string>::iterator it = keys.begin();
    std::string temp = *it;
    it++;
    for (; it != keys.end(); it++) {
        temp = temp + "," + *it;
    }
    fprintf(fd, "%s\n", temp.c_str());
    headerWritten = true;
}

void CSVLogger::logContent() {
    if (not headerWritten) logHeader();

    std::vector<std::string>::iterator it = keys.begin();
    std::string temp = umap[*it];
    it++;
    for (; it != keys.end(); it++) {
        temp = temp + "," + umap[*it];
    }
    fprintf(fd, "%s\n", temp.c_str());
}
