#include <iostream>
#include <cstdio>
#include <cstring>
#include <unistd.h>
#include "../inc/uart.hpp"
#include "../inc/packet.hpp"

void asciiToIntByte(char* dest, char* src) {
    dest[0] = src[0] - 48;
    dest[1] = src[1] - 48;
    dest[2] = src[2] - 48;
    dest[3] = src[3] - 48;
}

Packet* Packet::createPacket(int dt, void* data, char* mat, Code c) {
    Packet *p = NULL;

    switch(dt) {
        case(INT):
            p = new IntPacket(c, mat, *(int*)data);
            break;
        case(FLOAT):
            p = new FloatPacket(c, mat, *(float*)data);
            break;
        case(STRING):
            p = new StringPacket(c, mat, (char*)data);
            break;
        case(BYTE):
            p = new BytePacket(c, mat, *(char*)data);
            break;
        case(NONE):
            p = createPacket(c, mat);
            break;
    }

    return p;
}

Packet* Packet::createPacket(Code c, char* mat) {
    return new Packet(c, mat);
}

void Packet::getData(void* d) {
    return;
}

Packet* Packet::unpack(int dt, char* buffer, Code c) {
    Packet *p;

    switch(dt) {
        case(INT):
            p = IntPacket::unpack((char*)"0000", buffer, c);
            break;
        case(FLOAT):
            p = FloatPacket::unpack((char*)"0000", buffer, c);
            break;
        case(STRING):
            p = StringPacket::unpack((char*)"0000", buffer, c);
            break;
        case(BYTE):
            p = BytePacket::unpack((char*)"0000", buffer, c);
    }
    return p;
}

Packet* Packet::unpack(char* buffer) {
    int dt = getTypeFromCode((unsigned char)buffer[0]);
    return Packet::unpack(dt, &buffer[1], (Code)buffer[0]);
}

Packet::Packet(Code c, char* final_mat) {
    code = c;
    asciiToIntByte(mat, final_mat);
    memset(packetStream, 0, PACKETMAXLEN);
}

size_t Packet::pack() {
    int i = 0;
    packetStream[i++] = code;

    appendstring((void*)&packetStream[i], (void*)mat, MATLEN);
    i = i + MATLEN;

    i = i + insertData(&packetStream[i]);

    packetStreamLen = i;
    return i;
}

size_t Packet::insertData(char* stream) {
    return 0;
}

void Packet::getMessage(char* buffer) {
    memcpy(buffer, packetStream, packetStreamLen);
}

char* Packet::getMessage() {
    return packetStream;
}

void Packet::setCode(Code c) {
    code = c;
}

void Packet::print() {
    printf("Código: %x\n", code);

    printData();

//     printf("matricula: ");
//     for (int i = 0; i < 4; i++) {
//         printf("%d", mat[i]);
//     }
//     printf("\n");
}

void Packet::printPacketStream() {
    for (int i = 0; i < PACKETMAXLEN; i++) {
        printf("%x ", packetStream[i]);
    }
    printf("\n");
}

void Packet::printData() {
    return;
}

IntPacket::IntPacket(Code c, char* final_mat, int value) : Packet(c, final_mat) {
    data = value;
}

size_t IntPacket::insertData(char* stream) {
    appendstring((void*)stream, (void*)&data, sizeof(int));
    return sizeof(int);
}

void IntPacket::printData() {
    printf("Inteiro: %d\n", data);
}

IntPacket* IntPacket::unpack(char* final_mat, char* buffer, Code c) {
    int t;
    memcpy(&t, buffer, sizeof(int));
    return new IntPacket(c, final_mat, t);
}

FloatPacket::FloatPacket(Code c, char* final_mat, float value) : Packet(c, final_mat) {
    data = value;
}

size_t FloatPacket::insertData(char* stream) {
    appendstring((void*)stream, (void*)&data, sizeof(float));
    return sizeof(float);
}

void FloatPacket::printData() {
    printf("Float: %f\n", data);
}

FloatPacket* FloatPacket::unpack(char* final_mat, char* buffer, Code c) {
    float t;
    memcpy(&t, buffer, sizeof(float));
    return new FloatPacket(c, final_mat, t);
}

StringPacket::StringPacket(Code c, char* final_mat, const char* text) : Packet(c, final_mat) {
    memset(data, 0, STRINGMAXLEN);
    memccpy(data, text, 0, STRINGMAXLEN);
    dataLen = strlen(data);
}

StringPacket::StringPacket(Code c, char* final_mat, const char* text, unsigned int size) : Packet(c, final_mat) {
    memset(data, 0, STRINGMAXLEN);
    memccpy(data, text, 0, STRINGMAXLEN);
    dataLen = size;
}

size_t StringPacket::insertData(char* stream) {
    stream[0] = dataLen;
    appendstring((void*)&stream[1], (void*)data, dataLen);
    return dataLen + 1;
}

void StringPacket::printData() {
    printf("Tamanho da string: %u\n", dataLen);
    printf("String: [%s]\n", data);
}

StringPacket* StringPacket::unpack(char* final_mat, char* buffer, Code c) {
    return new StringPacket(c, final_mat, &buffer[1], buffer[0]);
}

BytePacket::BytePacket(Code c, char* final_mat, unsigned char value) : Packet(c, final_mat) {
    data = value;
}

size_t BytePacket::insertData(char* stream) {
    appendstring((void*)stream, (void*)&data, sizeof(unsigned char));
//     stream[0] = data;
    return sizeof(unsigned char);
}

void BytePacket::printData() {
    printf("Byte: %x\n", data);
}

BytePacket* BytePacket::unpack(char* final_mat, char* buffer, Code c) {
    return new BytePacket(c, final_mat, buffer[0]);
}
