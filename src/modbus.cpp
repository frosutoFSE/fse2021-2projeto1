#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "../inc/modbus.hpp"

extern "C" {
#include "../inc/crc16.h"
}

Modbus::Modbus(char address, FuncCode fc, char* data, size_t dataLen) {
    memset(dataBuffer, 0, PACKETMAXLEN);
    memset(completeBuffer, 0, MODBUSMAXLEN);
    addr = address;
    functionCode = fc;
    memcpy(dataBuffer, data, dataLen);
    dataBufferLen = dataLen;
}

Modbus::Modbus(FuncCode fc, char* data, size_t dataLen) {
    memset(dataBuffer, 0, PACKETMAXLEN);
    memset(completeBuffer, 0, MODBUSMAXLEN);
    addr = 0x01;
    functionCode = fc;
    memcpy(dataBuffer, data, dataLen);
    dataBufferLen = dataLen;
}

Modbus::Modbus(char* buffer, size_t len) {
    completeBufferLen = len;
    memcpy(completeBuffer, buffer, len);
    addr = buffer[0];
    functionCode = buffer[1];
    dataBufferLen = len-2-2; // len - len_crc - index_databuffer;
    memcpy(dataBuffer, &buffer[2], dataBufferLen);
    memcpy(crc, &buffer[len-2], 2);

    verifyCRC(buffer, len-2);
}

size_t Modbus::pack() {
    int i = 0;
//     append end
    completeBuffer[i++] = addr;
//     append function code
    completeBuffer[i++] = functionCode;
//     append data
    appendstring(&completeBuffer[i], dataBuffer, dataBufferLen);
    i = i + dataBufferLen;
//     calculate and append crc
    short crc = getCRC(i);
    appendstring(&completeBuffer[i], &crc, sizeof(short));
    i = i + sizeof(short);

    completeBufferLen = i;
    return i;
}

short Modbus::getCRC(size_t size) {
    return calcula_CRC((unsigned char*)completeBuffer, size);
}

void Modbus::verifyCRC(char* buffer, size_t len_without_crc) {
    short temp_crc = calcula_CRC((unsigned char*)buffer, len_without_crc);
    if (temp_crc != *(short*)crc) {
        error = crcError;
    }
}

char Modbus::verifyHeader(char expAddr, char expfunctionCode) {
    if (error == crcError) {
       return crcError;
    }
    if (addr != expAddr) {
        error = addrError;
        return addrError;
    }
    else if (expfunctionCode != functionCode) {
        error = funcCodeError;
        return funcCodeError;
    }
    return noError;
}

void Modbus::getMessage(char* buffer) {
    memcpy(buffer, completeBuffer, completeBufferLen);
}

char* Modbus::getMessage() {
    return completeBuffer;
}

void Modbus::getDataBuffer(char* buffer) {
    memcpy(buffer, dataBuffer, dataBufferLen);
}

char* Modbus::getDataBuffer() {
    return dataBuffer;
}

size_t Modbus::getDataBufferLen() {
    return dataBufferLen;
}
