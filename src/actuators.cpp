#include <cstdio>
#include <cstdlib>

#include <wiringPi.h>
#include <softPwm.h>
#include "../inc/actuators.hpp"

// Physical 16 / GPIO 23 / wiringPi 4
#define RESISTOR 4

// Physical 18 / GPIO 24 / wiringPi 5
#define VENTOINHA 5

#define RESMIN 0
#define RESMAX 100
#define VENTOFF 0
#define VENTONMIN 40
#define VENTONMAX 100

void activateResistor(int intensity) {
    if (intensity > VENTONMAX) {
        intensity = VENTONMAX;
    }
    softPwmWrite(RESISTOR, intensity);
}

void deactivateResitor() {
    softPwmWrite(RESISTOR, 0);
}

void activateCooler(int intensity) {
    if (intensity > 0 && intensity < VENTONMIN) {
        intensity = VENTONMIN;
    }
    if (intensity > VENTONMAX) {
        intensity = VENTONMAX;
    }
    softPwmWrite(VENTOINHA, intensity);
}

void deactivateCooler() {
    softPwmWrite(VENTOINHA, 0);
}

namespace act {

void updateIntensity(const int intensity) {
    int realIntensity = intensity < 0 ? intensity * -1 : intensity;
    if (intensity < 0) {
        // ativa Ventoinha
        deactivateResitor();
        activateCooler(realIntensity);
    }
    else if (intensity > 0) {
        // ativa Resistor
        deactivateCooler();
        activateResistor(realIntensity);
    }
    else {
        // intensidade é 0, logo os dois são desativados
        deactivateResitor();
        deactivateCooler();
    }
}

int setup() {
//     if (wiringPiSetup() != 0)
    pinMode(RESISTOR, OUTPUT);
    pinMode(VENTOINHA, OUTPUT);
    int res1 = softPwmCreate(RESISTOR, 0, RESMAX);
    int res2 = softPwmCreate(VENTOINHA, 0, VENTONMAX);
    int res = res1 || res2;
    // Qualquer coisa diferente de 0 é um erro
    if (res) {
        return -1;
    }
    updateIntensity(0);
    return res;
}

void close() {
    updateIntensity(0);
}

}
