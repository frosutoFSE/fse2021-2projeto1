#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

#include <vector>
#include <utility>
#include <unordered_map>
#include <string>
#include <sstream>

template <typename T>
std::string convertToString(T x) {
    std::stringstream s;
    s << x;
    return s.str();
}

class CSVLogger {
private:
    std::string path;
    FILE* fd;
    std::unordered_map<std::string, std::string> umap;
    std::vector<std::string> keys;
    bool headerWritten;

public:
    CSVLogger();
    CSVLogger(std::vector<std::string>& headers);
    ~CSVLogger();

    int openFile(std::string filepath, char* mode = (char*)"w+");

    void setHeaders(std::vector<std::string>& headers);
//     void setContent(std::unordered_map<std::string, std::string>& content);
    void setContent(std::pair<std::string, std::string>& content);
    void setContent(std::vector<std::pair<std::string, std::string>>& content);

    void logHeader();
    void logContent();

};

#endif
