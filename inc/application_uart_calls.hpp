#ifndef _APPLICATION_UART_CALLS_
#define _APPLICATION_UART_CALLS_

namespace uart {

int init(char* mat);
void close();
float requestPotentiometerTemperature();
float requestInternalTemperature();
int requestUserCommand();
void sendControlSignal(int controlSignal);
void sendReferenceTemperature(float TR);
int sendSystemState(char st);
int sendControlMode(char mode);

}
#endif
