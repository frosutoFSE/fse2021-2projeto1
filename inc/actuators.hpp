#ifndef _ACTUATORS_HPP_
#define _ACTUATORS_HPP_

namespace act {

int setup();
void updateIntensity(const int intensity);
void close();

}
#endif
