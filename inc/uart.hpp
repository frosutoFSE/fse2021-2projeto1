#ifndef UART_H
#define UART_H

#include <cstring>
#include <termios.h>

int UARTinit();
int UARTsend(void* message, size_t nbyte);
int UARTrecv(void* buffer, size_t nbyte);
void UARTclose();

class UART {

private:
    int fs;
    struct termios options;
    unsigned char* tx;
    unsigned char* rx;

    void config(int fs);

public:
    UART();
    ~UART();
    int init();
    int send(void* message, size_t nbyte);
    int recv(void* buffer, size_t nbyte);
};

#endif
