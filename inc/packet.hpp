#ifndef PACKET_H
#define PACKET_H

#include <cstring>

#define CODELEN 1
#define MATLEN 4
#define STRINGMAXLEN 257

const int PACKETMAXLEN = CODELEN + MATLEN + STRINGMAXLEN;

inline void appendstring(void* dest, void* src, size_t nbytes) {
    memcpy(dest, src, nbytes);
}

enum Code {
    RET = 0,
    RQ1 = 0xA1,
    RQ2 = 0xA2,
    RQ3 = 0xA3,
    SD1 = 0xB1,
    SD2 = 0xB2,
    SD3 = 0xB3,
    TIR = 0xC1, // request de temperatura interna
    TPR = 0xC2, // request de temperatura do potenciometro
    USR = 0xC3, // request de comando de usuário
    SCR = 0xD1, // envio do sinal de controle
    STR = 0xD2, // envio da temperatura de referência
    SST = 0xD3, // envio estado do sistema
    CMO = 0xD4, // envio modo de controle
};

enum DataType {
    INT, FLOAT, STRING, BYTE, NONE
};

inline DataType getTypeFromCode(unsigned char code) {

    DataType c;

//     if/else aqui teria menos linhas, mas switch é melhor otimizado :)
    switch (code) {
        case (TIR):
            c = FLOAT;
            break;
        case (TPR):
            c = FLOAT;
            break;
        case (USR):
            c = INT;
            break;
        case (SCR):
            c = INT;
            break;
        case (SST):
            c = INT;
            break;
        case (CMO):
            c = INT;
            break;
        case (RQ1):
            c = INT;
            break;
        case (RQ2):
            c = FLOAT;
            break;
        case (RQ3):
            c = STRING;
            break;
        case (SD1):
            c = INT;
            break;
        case (SD2):
            c = FLOAT;
            break;
        case (SD3):
            c = STRING;
            break;
        default:
            c = NONE;
            break;
    }

    return c;
}

class Packet {
/**
 * Base class to creation of packets
 */
private:
    Code code;
    char mat[4];
    char packetStream[PACKETMAXLEN];
    size_t packetStreamLen;

public:
    Packet(Code, char*);
    virtual ~Packet() {};
    void getMessage(char* buffer);
    char* getMessage();
    void print();
    void printPacketStream();
    void setCode(Code);

    virtual void getData(void*);
    virtual size_t pack();
    virtual size_t insertData(char*);
    virtual void printData();

    static Packet* createPacket(int dt, void* data, char* mat, Code c);
    static Packet* createPacket(Code c, char* mat);
    static Packet* unpack(int dt, char* buffer, Code c = RET);
    static Packet* unpack(char* buffer);
};

class IntPacket : public Packet {
private:
    int data;

public:
    IntPacket(Code, char*, int);
    ~IntPacket() {};
    size_t insertData(char*);

    inline void getData(void* d) {
        *(int*)d = data;
    }

    void printData();
    static IntPacket* unpack(char* final_mat, char* buffer, Code c = RET);
};

class FloatPacket : public Packet {
private:
    float data;

public:
    FloatPacket(Code, char*, float);
    ~FloatPacket() {};
    size_t insertData(char*);

    inline void getData(void* d) {
        *(float*)d = data;
    }

    void printData();
    static FloatPacket* unpack(char* final_mat, char* buffer, Code c = RET);
};

class StringPacket : public Packet {
private:
    unsigned int dataLen;
    char data[STRINGMAXLEN];

public:
    StringPacket(Code, char* mat, const char* data);
    StringPacket(Code, char* mat, const char* data, unsigned int dataLen);
    ~StringPacket() {};
    size_t insertData(char* stream);

    inline void getData(void* d) {
         d = data;
    }

    void printData();
    static StringPacket* unpack(char* final_mat, char* buffer, Code c = RET);
};

class BytePacket : public Packet {
private:
    unsigned char data;

public:
    BytePacket(Code c, char* mat, unsigned char value);
    ~BytePacket() {};
    size_t insertData(char* stream);

    inline void getData(void* d) {
        *(char*)d = data;
    }

    void printData();
    static BytePacket* unpack(char* final_mat, char* buffer, Code c = RET);
};

#endif
