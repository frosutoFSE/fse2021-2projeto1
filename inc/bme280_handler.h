#ifndef _BME280_HANDLER_H_
#define _BME280_HANDLER_H_

#include "bme280_defs.h"

/*
 * Inicializa as configurações básicas para o funcionamento do sensor
 */
int BMEH_setup();

/*
 * Encerra recursos usados pelo módulo do sensor
 */
void BMEH_close();

/*
 * seta sensor para operação no modo normal
 */
int BMEH_configure_sensor_to_normal_mode();

/*
 * Pega os dados do sensor. Caso alguma das informações não seja desejada, pode passar NULL no parâmetro
 */
int BMEH_get_sensor_data(float* temperature, float* pressure, float* humidity);

#endif
