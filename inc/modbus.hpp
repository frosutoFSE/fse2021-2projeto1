#ifndef MODBUS_HPP
#define MODBUS_HPP

#include "../inc/packet.hpp"

const int MODBUSMAXLEN = PACKETMAXLEN + 4;

enum FuncCode {
    SOLICITACAO = 0x23,
    ENVIO = 0x16
};

enum ErrorStatus {
    noError = 0, crcError = 1, addrError = 2, funcCodeError = 3
};

class Modbus {
private:
    char addr;
    char functionCode;
    char dataBuffer[PACKETMAXLEN];
    size_t dataBufferLen;
    char crc[2];
    char completeBuffer[MODBUSMAXLEN];
    size_t completeBufferLen;
    char error;

    short getCRC(size_t);
    void verifyCRC(char* buffer, size_t len_without_crc);

public:
    Modbus(char addr, FuncCode fc, char* data, size_t dataLen);
    Modbus(FuncCode fc, char* data, size_t dataLen);
    Modbus(char* buffer, size_t len);
    char* getMessage();
    void getMessage(char* buffer);
    void getDataBuffer(char* buffer);
    char* getDataBuffer();
    size_t getDataBufferLen();
    size_t pack();

    inline size_t getMessageLen() {
        return completeBufferLen;
    }

    char verifyHeader(char expAddr, char expfunctionCode);
    char getErrorStatus();
//     static Modbus* unpack();
};

#endif
