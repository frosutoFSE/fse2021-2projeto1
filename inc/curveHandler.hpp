#ifndef _CURVE_HANDLER_HPP_
#define _CURVE_HANDLER_HPP_

namespace refcurve {
    int setup(char* filename);
    void close();
    void readFile();
    float getReferenceValue();
    void startTimer();
//     void setCurveMode(bool flag);
}

#endif
