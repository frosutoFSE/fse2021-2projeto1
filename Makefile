CC = gcc
CXX = g++

ROOTDIR = .
INPUTDIR = $(ROOTDIR)/ref_files
CURVEFILE = $(INPUTDIR)/curva_reflow.csv
SRCDIR = $(ROOTDIR)/src
TESTSRCDIR = $(ROOTDIR)/testsrc
INCDIR = $(ROOTDIR)/inc
OBJDIR = $(ROOTDIR)/obj
BINDIR = $(ROOTDIR)/bin
BIN = main

CFLAGS = -c -Wall -I$(INCDIR)
LDFLAGS = -lwiringPi -pthread
TARGET = $(BINDIR)/$(BIN)

MAINOBJ = $(OBJDIR)/$(BIN).o
COBJ = $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(wildcard $(SRCDIR)/*.c))
CXXOBJ = $(filter-out $(MAINOBJ), $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(wildcard $(SRCDIR)/*.cpp)))
OBJ = $(COBJ) $(CXXOBJ)

$(TARGET) : $(OBJ) $(MAINOBJ)
	    $(CXX) $^ -o $@ $(LDFLAGS)

$(OBJDIR)/%.o : $(SRCDIR)/%.c
		$(CC) $(CFLAGS) $< -o $@

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
		$(CXX) $(CFLAGS) $< -o $@

tests: $(OBJ)
	$(CC)  $(COBJ)  $(TESTSRCDIR)/lcd_test.c              -o $(BINDIR)/lcd_test            $(LDFLAGS)
	$(CXX) $(OBJ)   $(TESTSRCDIR)/logger_test.cpp         -o $(BINDIR)/logger_test         $(LDFLAGS)
	$(CXX) $(OBJ)   $(TESTSRCDIR)/teste_modbus_proj.cpp   -o $(BINDIR)/modbus_test         $(LDFLAGS)
	$(CXX) $(OBJ)   $(TESTSRCDIR)/test_curveHandler.cpp   -o $(BINDIR)/curve_test          $(LDFLAGS)
	$(CXX) $(OBJ)   $(TESTSRCDIR)/desativar_atuadores.cpp -o $(BINDIR)/desativar_atuadores $(LDFLAGS)

clean:
	rm -f $(OBJDIR)/* $(BINDIR)/*

run:
	@$(TARGET) $(CURVEFILE)

prepare:
	@mkdir -p $(BINDIR) $(OBJDIR)

.PHONY: test_bin, clean, run, prepare
