#include <cstdio>
#include <cstdlib>
#include <string>
#include <unistd.h>

#include "../inc/application_uart_calls.hpp"

int main() {

    uart::init((char*)"9177");

    float pot = uart::requestPotentiometerTemperature();
    printf("Potenciometro: %.2f\n", pot);

    float ti = uart::requestInternalTemperature();
    printf("Temperatura interna: %.2f\n", ti);

    int usercmd = uart::requestUserCommand();
    printf("Comando: %d\n", usercmd);

    uart::sendControlSignal(20);
    printf("Sinal de controle enviado\n");

    uart::sendReferenceTemperature(38.0);
    printf("Temperatura referencia enviada\n");

    int sst = uart::sendSystemState((char)0);
    printf("Estado do sistema retornado: %d\n", sst);

    int mode = uart::sendControlMode((char)0);
    printf("Modo de controle: %d\n", mode);

    uart::close();
    return 0;
}
