#include <stdio.h>
#include <stdlib.h>
#include "../inc/i2clcd.h"

int main(int argc, char** argv) {

    if (argc < 3) {
        printf("Uso: <bin_name> <string a enviar para o led> <linha>\n");
        exit(0);
    }

    int line = atoi(argv[2]);
    if (line < 1 || line > 2) {
        printf("linha deve ser 1 ou 2\n");
        exit(1);
    }

    if (lcd_setup() != 0) {
        printf("Erro na inicialização do lcd\n");
        exit(1);
    }

    if (line == 1) {
        line = LINE1;
    }
    else if (line == 2) {
        line = LINE2;
    }

    ClrLcd();
    lcdLoc(line);
    typeln(argv[1]);

    lcd_close();

    return 0;
}
