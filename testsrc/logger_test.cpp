#include <cstdio>
#include <vector>
#include <utility>
#include <string>
#include <sstream>

#include "../inc/logger.hpp"

typedef std::pair<std::string, std::string> logPair;

int main() {

    std::vector<std::string> headers;
    headers.emplace_back("header1");
    headers.emplace_back("header2");
    headers.emplace_back("header42");

    CSVLogger logger = CSVLogger(headers);

    if (logger.openFile("/tmp/logger_teste") < 0) {
        printf("Erro ao abrir arquivo\n");
        exit(0);
    }

    std::vector<logPair> v;

    v.emplace_back(logPair("header1", convertToString<float>(231.42)));
    v.emplace_back(logPair("header2", convertToString<int>(50000)));
    v.emplace_back(logPair("header42", "Just a string"));

    logger.setContent(v);

    logger.logContent();

    return 0;
}
