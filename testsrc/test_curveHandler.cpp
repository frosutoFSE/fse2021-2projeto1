#include "../inc/curveHandler.hpp"

#include <cstdio>
#include <cstdlib>
#include <unistd.h>

int main() {

    char filename[] = "ref_files/curva_reflow.csv";

    if (refcurve::setup(filename) != 0) {
        printf("Erro ao abrir o arquivo\n");
        exit(0);
    }

    refcurve::readFile();

    refcurve::startTimer();

    int seccount = 0;
    while (seccount < 130) {

        printf("%.2f\n", refcurve::getReferenceValue());

        sleep(1);
        seccount++;
    }

    refcurve::close();

    return 0;
}
