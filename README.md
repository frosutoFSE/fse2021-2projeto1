# FSE 2021.2 - Projeto 1

## Identificação

**Nome**: Wagner Martins da Cunha

**Matrícula**: 18/0029177

## Sobre

Repositório de desenvolvimento do [trabalho 1](https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2) de Fundamentos de Sistemas Embarcados.

O programa simula o controle de um forno para soldagem de placas de circuito impresso. Mais informações no [enunciado](https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2).

## Compilação

A compilação do programa pode ser feita via make na Raspberry Pi. Para a compilação, é assumido que todas as pastas ["src", "inc", "obj", "bin", "testsrc" e "ref_files"] existam no sistema de arquivos, assim como o [Makefile](./Makefile). A raspberry já deve ter instalada a biblioteca [wiringPi](http://wiringpi.com/).

Todos os comandos usando **make** devem ser usados na mesma pasta que se encontra o arquivo [Makefile](./Makefile).

Para compilar, utilize o comando:

```sh
make
```

e será gerado o arquivo **./bin/main**. Todos os arquivos gerados na compilação podem ser removidos com:

```sh
make clean
```

## Execução

Para executar o programa, utilize o comando:

```sh
make run
```

Para executar o binário diretamente, é necessário usar como argumento o caminho para o arquivo [curva_reflow.csv](./ref_files/curva_reflow.csv). Por exemplo, a partir do diretório raiz:

```sh
./bin/main ./ref_files/curva_reflow.csv
```

## Arquivos de teste

Ao utilizar o comando:

```sh
make tests
```

serão gerados binários para testar diferentes módulos do programa a partir dos arquivos de código fonte da pasta `testsrc`. Para executar estes binários, eles devem ser chamados diretamente, e ficarão na pasta `bin`. Vale ressaltar que não são testes unitários, cada binário é um programa diferente com seu próprio contexto de uso.

#### Tabela de uso aqui
|Binário gerado|Uso|Descrição|
|--------------|---|---------|
|lcd_test|bin/lcd_test \<string a enviar para o led\> \<linha\>|Atualiza o lcd com a string enviada|
|logger_test|bin/logger_test|Cria um arquivo de log definido em [logger_test.cpp](./testsrc/logger_test.cpp)|
|modbus_test|bin/modbus_test|Envia todos os pacotes MODBUS definidos no trabalho para a esp com valores setados em [teste_modbus_proj.cpp](./testsrc/teste_modbus_proj.cpp)|
|curve_test|bin/curve_test|Segue o arquivo [curva_reflow.csv](./ref_files/curva_reflow.csv), logando o valor atual da temperatura de referência a cada 1 segundo por 130 segundos.|
|desativar_atuadores|bin/desativar_atuadores|Desliga os dois atuadores (seta pwm para 0)|

## Experimentos

Um arquivo de log é criado a cada execução do programa. Caso o arquivo já exista, ele será sobreescrito na próxima execução. O log salva as seguintes informações:

|Dado|Tipo ou Formatação|
|:--:|:-----------------|
|Data | dd/mm/aa
|Hora | hh:mm:ss
|Modo de Controle | curva "CUR", potenciômetro "POT"
|Temperatura Ambiente | float
|Temperatura Interna  | float
|Temperatura Referencia | float
|Acionamento % | %

Foram feitos dois experimentos, ambos usando o método de controle PID(Proporcional Integral Derivativo), um usando o potenciômetro para capturar a temperatura referência, e outro usando a curva de reflow disponibilizada pelo professor, que se encontra no arquivo [curva_reflow.csv](ref_files/curva_reflow.csv), cada um executado por aproximadamente 10 minutos na plataforma rasp42. Abaixo encontram-se os gráficos de temperatura e acionamento de ambos os experimentos. Para a geração destes gráficos, é usado o [notebook](./experiments/experiment_charts.ipynb).

### Potenciômetro

#### Gráfico de Temperatura

![potenciômetro_temperatura](./experiments/potenciometro_temperatura.jpg)

#### Gráfico de Acionamento

![potenciômetro_acionamento](./experiments/potenciometro_acionamento.jpg)

### Curva de Reflow

#### Gráfico de Temperatura

![curva_temperatura](./experiments/curva_temperatura.jpg)

#### Gráfico de Acionamento

![curva_acionamento](./experiments/curva_acionamento.jpg)

### Conclusão dos experimentos

O programa salva o log e atualiza o LCD com as informações do sistema a cada atualização completa do fluxo escolhido (curva ou potenciômetro). Ele também só é salvo enquanto o sistema estiver no estado ligado. Para chamadas via UART que esperam uma resposta, o programa espera 1 segundo para fazer a leitura e por este motivo, ambos log e LCD são atualizados num tempo maior que 1 segundo. Mais precisamente, no modo de referência do potenciômetro, cada registro é salvo a cada ~3 segundos, enquanto no modo de curva, cada registro é salvo a cada ~2 segundos.

Como é possível observar no gráfico da curva, o sistema seguiu a curva, mas devido a limitações na potência dos atuadores, a temperatura interna chegou poucas vezes na temperatura de referência.
No experimento do potenciômetro, a temperatura interna ficou próxima da referência por algum tempo, devido a menor variação desta. No período que a temperatura esteve mais próxima, a temperatura interna teve uma tendência a ficar a aproximadamente 1 ou 2 segundos acima da referência. Este resultado provavelmente obteria melhorias caso o tempo de espera de resposta da UART fosse menor que 1 segundo, desta forma, possibilitando uma atualização mais rápida da intensidade dos atuadores.

Obs.: nos logs, o acionamento é salvo com o valor retornado pelo algoritmo de cálculo do PID, logo números entre -40 e 0, valores "proibidos" de acionamento da ventoinha, podem aparecer. Contudo, o módulo que gerencia o controle dos atuadores força o limite correto internamente, ou seja, o valor mandado no pwm da ventoinha sempre será -40, caso o resultado do PID esteja dentro da faixa entre -40 e 0.
